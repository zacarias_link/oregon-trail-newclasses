class Dog {
    constructor (name, breed, isGoodBoy){
        this.name = name;
        this.breed = breed;
        this.isGoodBoy = isGoodBoy;
    }
    
 }
 
 class NewDog extends Dog {
    constructor (sit, fetch){
        super(name, breed, isGoodBoy)
    }    
    sit() {
        // sitting code here
    }
    fetch() {
        // fetching code here
    }


 }
 class Traveler{
    constructor (name){
        this.name = name   
        this.food = 1
        this.isHealthy = true
        // this.hunt()
        // this.eat()
    }
    hunt() {
        this.food += 2 
    }
    eat(){
        if(this.food <= 0){
            this.isHealthy = false 
        }else{
            this.food -= 1
        }    
    }    
 }
 class Wagon {
    constructor (capacity) {
        this.capacity = Number(capacity)
        this.passagers = []
        // this.getAvailableSeatCount()
        // this.join()
        // this.shouldQuarantine()
        // this.totalFood()
    }
    getAvailableSeatCount() {
        return  (this.capacity - this.passagers.length) 
    }
    join() {
        if (this.capacity <= this.passagers.length){
            return "no space, sorry"
        }else{
            this.passagers.push(arguments)
        }
    }
    shouldQuarantine() {
        let output = false
        this.passagers.forEach(function checkHealthy(element, index, array){
            if(element[0].isHealthy === false){
                output = true
            }
        });
        
        return output
    }
    totalFood() {
        let travalersFood = 0
        this.passagers.forEach(function checkHealthy(element, index, array){
           travalersFood += element[0].food
        });
        return travalersFood
    } 
        
 }
 class Doctor extends Traveler{
    constructor(name, food, isHealthy){
        super(name, food, isHealthy)
    }
    heal(traveler) {
      
        traveler.isHealthy = true
        
    }
 }
 class Hunter extends Traveler{
    constructor(name, food, isHealthy){
        super(name, food, isHealthy)
        this.food++  
    }  
    
    hunt() {
        this.food += 5 
    }
    eat(){
        if(this.food <= 1){
            this.food = 0
            this.isHealthy = false 
        }else{
            this.food -= 2
        }    
    }
    giveFood(traveler, numOfFoodUnits){
        if (numOfFoodUnits > this.food){
            return "não tenho comida viajante"
        }else{
            traveler.food += numOfFoodUnits
            this.food -= numOfFoodUnits
        }
        
    } 
 }
//  Traveler.prototype = {
//     constructor: Traveler,
//     hunt: function () {
//         this.food += 2 
//     },
//     eat: function () {
//         if(this.food <= 0){
//             this.isHealthy = false 
//         }else{
//             this.food -= 1
//         }    
//     }
//  }
 
//  Wagon.prototype = {
//     constructor: Wagon,
//     getAvailableSeatCount: function () {
//         return (this.capacity - this.passagers.length) 
//     },
//     join: function () {
//         if (this.capacity <= this.passagers.length){
//             return "no space, sorry"
//         }else{
//             this.passagers.push(arguments)
//         }
//     },
//     shouldQuarantine: function () {
//         this.passagers.forEach(function checkHealthy(element, index, array){
          
//             if(element.isHealthy === false){
//                 return false
//             }
//         });
//         return true
//     },
//     totalFood: function () {
//         let travalersFood = 0
//         this.passagers.forEach(function checkHealthy(element, index, array){
//            travalersFood += element[0].food
//         });
//         return travalersFood
//     }
//  }
 
// Cria uma carroça que comporta 4 pessoas
let wagon = new Wagon(4);
// Cria cinco viajantes
let henrietta = new Traveler('Henrietta');
let juan = new Traveler('Juan');
let drsmith = new Doctor('Dr. Smith');
let sarahunter = new Hunter('Sara');
let maude = new Traveler('Maude');

console.log(`#1: There should be 4 available seats. Actual: ${wagon.getAvailableSeatCount()}`);

wagon.join(henrietta);
console.log(`#2: There should be 3 available seats. Actual: ${wagon.getAvailableSeatCount()}`);

wagon.join(juan);
wagon.join(drsmith);
wagon.join(sarahunter);

wagon.join(maude); // Não tem espaço para ela!
console.log(`#3: There should be 0 available seats. Actual: ${wagon.getAvailableSeatCount()}`);

console.log(`#4: There should be 5 total food. Actual: ${wagon.totalFood()}`);

sarahunter.hunt(); // pega mais 5 comidas
drsmith.hunt();

console.log(`#5: There should be 12 total food. Actual: ${wagon.totalFood()}`);

henrietta.eat();
sarahunter.eat();
drsmith.eat();
juan.eat();
juan.eat(); // juan agora está doente (sick)

console.log(`#6: Quarantine should be true. Actual: ${wagon.shouldQuarantine()}`);
console.log(`#7: There should be 7 total food. Actual: ${wagon.totalFood()}`);

drsmith.heal(juan);

console.log(`#8: Quarantine should be false. Actual: ${wagon.shouldQuarantine()}`);

sarahunter.giveFood(juan, 4);
sarahunter.eat(); // Ela só tem um, então ela come e fica doente

console.log(`#9: Quarantine should be true. Actual: ${wagon.shouldQuarantine()}`);
console.log(`#10: There should be 6 total food. Actual: ${wagon.totalFood()}`);